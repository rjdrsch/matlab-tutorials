# matlab-tutorials

MATLAB tutorials focused on Environmental Engineering applications.

## Tutorials

1. Basic: Nd-array operations (scalars, vectors, matrices)
2. Keeling: Tables, statistics, and linear models
