% myScript.m
% This script returns the dot product of two vectors
% 
x = [1 2 3 4];
y = [-1 -4 -9 -16]';

x * y